import java.io.IOException;
import java.io.StringReader;

import com.google.gson.stream.JsonReader;

/**
 * An example illustrating the use of the GSON library to parse and manipulate
 * JSON files.
 *
 * @author wangrace
 */
public class GsonExample {
    public static String text = "{\n"
	+ "  \"cis\": [\n"
	+ "    {\n"
        + "      \"name\": \"CIS120\",\n"
        + "      \"instructor\": \"Dr. Weirich\",\n"
        + "      \"languages\": [\"OCaml\", \"Java\"]\n"
	+ "    },\n"
        + "    {\n"
        + "      \"name\": \"CIS121\",\n"
        + "      \"instructor\": \"Dr. Gandhi\",\n"
        + "      \"languages\": [\"Java\"]\n"
        + "    }\n"
        + "  ]\n"
        + "}\n";

    public static void main(String args[]) throws IOException {
		JsonReader reader = new JsonReader(new StringReader(text));
		reader.beginObject();
		reader.skipValue(); // skip 'cis'
		reader.beginArray();
		while (reader.hasNext()) {
			reader.beginObject(); // begin reading the objects representing CIS120, CIS121

			reader.skipValue(); // skip 'name'
		    System.out.println("Course name: " + reader.nextString());

			reader.skipValue(); // skip 'instructor'
		    System.out.println("Instructor: " + reader.nextString());
		    System.out.println("Languages taught:");

		    reader.skipValue(); // skip languages'
		    reader.beginArray();
		    while (reader.hasNext()) {
		    	System.out.println("  - " + reader.nextString());
		    }
		    reader.endArray();

		    reader.endObject();
		}

		reader.close();
    }
}
