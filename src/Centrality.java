import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

/**
 * Centrality is a class that processes a graph to produce measures of
 * popularity.
 *
 * Do not change method headers that are provided.
 *
 * @author scheiber
 */
public class Centrality {

	/**
	 * Returns a mapping from each person in the graph to a double representing
	 * his or her betweenness centrality.
	 *
	 * @param graph
	 *            if graph is null, then throw an IllegalArgumentException
	 */
	public static Map<AbstractPerson, Double> betweennessCentrality(Set<AbstractPerson> graph) {
		if(graph == null){
			throw new IllegalArgumentException();
		}
		Map<AbstractPerson, Double> sigma;
		Map<AbstractPerson, Double> d;
		Map<AbstractPerson, Double> delta;
		Stack<AbstractPerson> S;
		Queue<AbstractPerson> Q;
		Map<AbstractPerson,ArrayList<AbstractPerson>> P;
		Map<AbstractPerson,Double> Cb = new HashMap<AbstractPerson,Double>();
		for(AbstractPerson a: graph){
			Cb.put(a, 0.0);
		}
		
		for(AbstractPerson s : graph){
			S = new Stack<AbstractPerson>();
			P = new HashMap<AbstractPerson,ArrayList<AbstractPerson>>();
			sigma = new HashMap<AbstractPerson, Double>();
			delta = new HashMap<AbstractPerson, Double>();
			d = new HashMap<AbstractPerson, Double>();
			for(AbstractPerson w : graph){
				ArrayList<AbstractPerson> pList = new ArrayList<AbstractPerson>();
				P.put(w, pList);
				if(w.equals(s)){
					sigma.put(s, 1.0);
					d.put(s, 0.0);
				} else {
					sigma.put(w, 0.0);
					d.put(w, -1.0);
				}
			}
			Q = new LinkedList<AbstractPerson>();
			Q.offer(s);
			while(!Q.isEmpty()){
				AbstractPerson v = Q.poll();
				S.push(v);
				for(AbstractPerson w : v.getFriends()){
					if(d.get(w) < 0){
						Q.add(w);
						d.put(w, d.get(v)+1);
					}
					if(d.get(w) == (d.get(v) + 1)){
						sigma.put(w, sigma.get(w) + sigma.get(v));
						P.get(w).add(v);
					}
				}
			}
			for(AbstractPerson b : graph){
				delta.put(b, 0.0);
			}
			
			while(!S.isEmpty()){
				AbstractPerson w = S.pop();
				for(AbstractPerson v : P.get(w)){
					delta.put(v, delta.get(v) + (sigma.get(v)/(sigma.get(w))*(1+delta.get(w))));
				}
				if(!w.equals(s)){
					Cb.put(w, Cb.get(w) + delta.get(w));
				}
			}
		}
		return Cb;
	}

	/**
	 * Returns a mapping from each person in the graph to his or her PageRank
	 * after the specified number of iterations.
	 *
	 * @param graph
	 *            if graph is null, then throw an IllegalArgumentException
     * @param iterations
     *            if iterations is negative, then throw an
     *            IllegalArgumentException
	 */
	public static Map<AbstractPerson, Double> pageRank(Set<AbstractPerson> graph,
			int iterations) {
		if((graph == null) || (iterations < 0)){
			throw new IllegalArgumentException();
		}
		Map<Integer, Map<AbstractPerson, Double>> rankHistory = new HashMap<Integer, Map<AbstractPerson, Double>>();
		Map<AbstractPerson, Double> startingRanks = new HashMap<AbstractPerson, Double>();
		for(AbstractPerson p : graph){
			startingRanks.put(p, 1.0);
		}
		rankHistory.put(0, startingRanks);
		for(int i = 1; i <= iterations; i++){
			Map<AbstractPerson, Double> ranks = new HashMap<AbstractPerson,Double>();
			for(AbstractPerson p : graph){
				double sum = 0;
				for(AbstractPerson q : p.getFollowers()){
					sum += rankHistory.get(i-1).get(q)/q.getFriends().size();
				}
				sum = sum * 0.85;
				ranks.put(p, .15+sum);
			}
			rankHistory.put(i, ranks);
		}
		return rankHistory.get(iterations);
	}
}
