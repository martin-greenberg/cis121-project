import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class GraphMakerTest {
	static GraphMaker maker;
	static Set<AbstractPerson> parsed;
	static Map<Integer, AbstractPerson> idMap;
	
	@BeforeClass
	public static void setUpClass() throws Exception {
		maker = new GraphMaker("small.json");
		parsed = maker.parse();
		idMap = new HashMap<Integer, AbstractPerson>();
		for(AbstractPerson p : parsed){
			idMap.put(p.getId(), p);
		}
	}

	@Test
	public void testNullInput(){
		assertNull((new GraphMaker(null)).parse());
	}
	
	@Test
	public void testPersons(){
		assertEquals(parsed.size(),9);
	}
	
	@Test
	public void testNames(){
		Set<String> names = new HashSet<String>();
		for(AbstractPerson p : parsed){
			names.add(p.getName());
		}
		assertTrue(names.contains("Lamont Oliver"));
		assertTrue(names.contains("Sharon Faulkner"));
		assertTrue(names.contains("Ashley Stansbery"));
		assertTrue(names.contains("Cruz Tucker"));
		assertTrue(names.contains("Mary Valone"));
		assertTrue(names.contains("Clarence Oaks"));
		assertTrue(names.contains("Jessica Brennan"));
		assertTrue(names.contains("Jeffrey Gralak"));
		assertTrue(names.contains("Ron Stutzman"));
	}
	
	@Test
	public void testFollowers0(){ //testFollowersN and testFriendsN will test the corresponding set for the Person with ID N
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(1));
		followers.add(idMap.get(3));
		followers.add(idMap.get(4));
		AbstractPerson p = idMap.get(0);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriends0(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(2));
		friends.add(idMap.get(1));
		friends.add(idMap.get(4));
		friends.add(idMap.get(3));
		AbstractPerson p = idMap.get(0);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers1(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(0));
		followers.add(idMap.get(2));
		followers.add(idMap.get(3));
		followers.add(idMap.get(4));
		AbstractPerson p = idMap.get(1);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriends1(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(0));
		friends.add(idMap.get(4));
		friends.add(idMap.get(2));
		AbstractPerson p = idMap.get(1);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers2(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(0));
		followers.add(idMap.get(1));
		AbstractPerson p = idMap.get(2);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriends2(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(1));
		friends.add(idMap.get(4));
		AbstractPerson p = idMap.get(2);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers3(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(0));
		AbstractPerson p = idMap.get(3);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriends3(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(0));
		friends.add(idMap.get(1));
		AbstractPerson p = idMap.get(3);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers4(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(0));
		followers.add(idMap.get(1));
		followers.add(idMap.get(2));
		AbstractPerson p = idMap.get(4);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriend4(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(0));
		friends.add(idMap.get(1));
		AbstractPerson p = idMap.get(4);
		assertTrue(friends.equals(p.getFriends()));
	}
	@Test
	public void testFollowers5(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(6));
		followers.add(idMap.get(7));
		followers.add(idMap.get(8));
		AbstractPerson p = idMap.get(5);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriends5(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(7));
		friends.add(idMap.get(8));
		friends.add(idMap.get(6));
		AbstractPerson p = idMap.get(5);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers6(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(5));
		followers.add(idMap.get(7));
		followers.add(idMap.get(8));
		AbstractPerson p = idMap.get(6);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriends6(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(5));
		friends.add(idMap.get(8));
		friends.add(idMap.get(7));
		AbstractPerson p = idMap.get(6);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers7(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(5));
		followers.add(idMap.get(6));
		followers.add(idMap.get(8));
		AbstractPerson p = idMap.get(7);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	
	@Test
	public void testFriends7(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(5));
		friends.add(idMap.get(8));
		friends.add(idMap.get(6));
		AbstractPerson p = idMap.get(7);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers8(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(5));
		followers.add(idMap.get(6));
		followers.add(idMap.get(7));
		AbstractPerson p = idMap.get(8);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	
	@Test
	public void testFriends8(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(5));
		friends.add(idMap.get(6));
		friends.add(idMap.get(7));
		AbstractPerson p = idMap.get(8);
		assertTrue(friends.equals(p.getFriends()));
	}
}
