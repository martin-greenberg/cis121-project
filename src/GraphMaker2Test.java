import static org.junit.Assert.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.junit.BeforeClass;
import org.junit.Test;


public class GraphMaker2Test {

	static GraphMaker2 maker;
	static Set<AbstractPerson> parsed;
	static Map<Integer, AbstractPerson> idMap;
	
	@BeforeClass
	public static void setUpClass() throws Exception {
		maker = new GraphMaker2("small_with_tweets.json");
		parsed = maker.parse();
		idMap = new HashMap<Integer, AbstractPerson>();
		for(AbstractPerson p : parsed){
			idMap.put(p.getId(), p);
		}
	}
	
	//some of the same tests as GraphMaker
	@Test
	public void testNullInput(){
		assertNull((new GraphMaker2(null)).parse());
	}
	
	@Test
	public void testPersons(){
		assertEquals(parsed.size(),9);
	}
	
	@Test
	public void testNames(){
		Set<String> names = new HashSet<String>();
		for(AbstractPerson p : parsed){
			names.add(p.getName());
		}
		assertTrue(names.contains("Lamont Oliver"));
		assertTrue(names.contains("Sharon Faulkner"));
		assertTrue(names.contains("Ashley Stansbery"));
		assertTrue(names.contains("Cruz Tucker"));
		assertTrue(names.contains("Mary Valone"));
		assertTrue(names.contains("Clarence Oaks"));
		assertTrue(names.contains("Jessica Brennan"));
		assertTrue(names.contains("Jeffrey Gralak"));
		assertTrue(names.contains("Ron Stutzman"));
	}
	
	@Test
	public void testFollowers0(){ //testFollowersN and testFriendsN will test the corresponding set for the Person with ID N
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(1));
		followers.add(idMap.get(3));
		followers.add(idMap.get(4));
		AbstractPerson p = idMap.get(0);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriends0(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(2));
		friends.add(idMap.get(1));
		friends.add(idMap.get(4));
		friends.add(idMap.get(3));
		AbstractPerson p = idMap.get(0);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers1(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(0));
		followers.add(idMap.get(2));
		followers.add(idMap.get(3));
		followers.add(idMap.get(4));
		AbstractPerson p = idMap.get(1);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriends1(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(0));
		friends.add(idMap.get(4));
		friends.add(idMap.get(2));
		AbstractPerson p = idMap.get(1);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers2(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(0));
		followers.add(idMap.get(1));
		AbstractPerson p = idMap.get(2);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriends2(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(1));
		friends.add(idMap.get(4));
		AbstractPerson p = idMap.get(2);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers3(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(0));
		AbstractPerson p = idMap.get(3);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriends3(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(0));
		friends.add(idMap.get(1));
		AbstractPerson p = idMap.get(3);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers4(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(0));
		followers.add(idMap.get(1));
		followers.add(idMap.get(2));
		AbstractPerson p = idMap.get(4);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriend4(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(0));
		friends.add(idMap.get(1));
		AbstractPerson p = idMap.get(4);
		assertTrue(friends.equals(p.getFriends()));
	}
	@Test
	public void testFollowers5(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(6));
		followers.add(idMap.get(7));
		followers.add(idMap.get(8));
		AbstractPerson p = idMap.get(5);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriends5(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(7));
		friends.add(idMap.get(8));
		friends.add(idMap.get(6));
		AbstractPerson p = idMap.get(5);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers6(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(5));
		followers.add(idMap.get(7));
		followers.add(idMap.get(8));
		AbstractPerson p = idMap.get(6);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	@Test
	public void testFriends6(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(5));
		friends.add(idMap.get(8));
		friends.add(idMap.get(7));
		AbstractPerson p = idMap.get(6);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers7(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(5));
		followers.add(idMap.get(6));
		followers.add(idMap.get(8));
		AbstractPerson p = idMap.get(7);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	
	@Test
	public void testFriends7(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(5));
		friends.add(idMap.get(8));
		friends.add(idMap.get(6));
		AbstractPerson p = idMap.get(7);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testFollowers8(){
		Set<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(idMap.get(5));
		followers.add(idMap.get(6));
		followers.add(idMap.get(7));
		AbstractPerson p = idMap.get(8);
		assertTrue(followers.equals(p.getFollowers()));
	}
	
	
	@Test
	public void testFriends8(){
		Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(idMap.get(5));
		friends.add(idMap.get(6));
		friends.add(idMap.get(7));
		AbstractPerson p = idMap.get(8);
		assertTrue(friends.equals(p.getFriends()));
	}
	
	@Test
	public void testExtractHashtagsNullString(){
		assertEquals(GraphMaker2.extractHashtags(null), new HashSet<String>());
	}
	
	@Test
	public void testExtractHashtagsEmptyString(){
		assertEquals(GraphMaker2.extractHashtags(""), new HashSet<String>());
	}
	
	@Test
	public void testExtractHashtagsOneTag(){
		Set<String> expected = new HashSet<String>();
		expected.add("CIS121");
		assertEquals(GraphMaker2.extractHashtags("#CIS121"), expected);
	}
	
	@Test
	public void testExtractHashtagsTwoTags(){
		Set<String> expected = new HashSet<String>();
		expected.add("CIS121");
		expected.add("hashtag");
		assertEquals(GraphMaker2.extractHashtags("#CIS121 #hashtag"), expected);
	}
	
	@Test
	public void testExtractHashtagsTagsAndContent(){
		Set<String> expected = new HashSet<String>();
		expected.add("CIS121");
		expected.add("hashtag");
		assertEquals(GraphMaker2.extractHashtags("It was the best of #CIS121 times, it was the worst of #hashtag times"), expected);
	}
	
	@Test
	public void testExtractHashtagsBlendedNonAlphanumeric(){
		Set<String> expected = new HashSet<String>();
		expected.add("CIS121");
		expected.add("hashtag");
		assertEquals(GraphMaker2.extractHashtags("It was the best of #CIS121!times, it was the worst of #hashtag-times"), expected);
	}
	
	@Test
	public void testExtractHashtagsCaseSensitivity(){
		Set<String> expected = new HashSet<String>();
		expected.add("CIS121");
		expected.add("cis121");
		assertEquals(GraphMaker2.extractHashtags("#CIS121 #cis121"), expected);
	}
	
	@Test
	public void testGetGraphMap(){
		assertEquals(maker.getGraphMap(), idMap);
	}
}
