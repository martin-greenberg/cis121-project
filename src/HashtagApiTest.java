import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;


public class HashtagApiTest {

	static HashtagApi api;
	
	@BeforeClass
	public static void setUpClass() throws Exception {
		api = new HashtagApi("small_with_tweets.json");
	}

	@Test (expected = IllegalArgumentException.class)
	public void testGetTweetsByPersonNull() {
		api.getTweetsByPerson(null);
	}
	
	@Test
	public void testGetTweetsByPersonNotInGraph() {
		assertEquals(api.getTweetsByPerson(new Person(300)), new HashSet<Integer>());
	}

	@Test
	public void testGetTweetsByPersonInGraph() {
		HashSet<Integer> expected = new HashSet<Integer>();
		expected.add(0);
		expected.add(1);
		expected.add(2);
		expected.add(3);
		expected.add(4);
		expected.add(5);
		expected.add(6);
		AbstractPerson p = api.getGraphObject().getGraphMap().get(0);
		assertEquals(api.getTweetsByPerson(p), expected);
	}
	
	@Test
	public void testGetTweetsWithHashtagEmpty() {
		assertEquals(api.getTweetsWithHashtag(""), new HashSet<Integer>());
	}
	
	@Test
	public void testGetTweetsWithHashtagNull() {
		assertEquals(api.getTweetsWithHashtag(null), new HashSet<Integer>());
	}
	
	@Test
	public void testGetTweetsWithHashtagValid() {
		HashSet<Integer> expected = new HashSet<Integer>();
		expected.add(6);
		expected.add(10);
		expected.add(16);
		expected.add(28);
		expected.add(30);
		expected.add(33);
		expected.add(36);
		expected.add(39);
		expected.add(42);
		expected.add(48);
		expected.add(49);
		expected.add(52);
		expected.add(54);
		assertEquals(api.getTweetsWithHashtag("#ValRocks"), expected);
	}
}
