import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.gson.stream.JsonReader;

/**
 * A class that parses a data file in JSON form into a graph representation.
 * 
 * @author wangrace
 * 
 */
public class GraphMaker {
	private Map<Integer,AbstractPerson> graph;
	JsonReader reader;

	public GraphMaker(String filename){
		if(filename == null){
			System.out.println("GraphMaker null filename input");
		} else {
			graph = new HashMap<Integer,AbstractPerson>();
			try {
				reader = new JsonReader(new FileReader(filename));
			} catch (FileNotFoundException e) {
				System.out.println("GraphMaker FileNotFoundException");
			}
		}
	}

	/**
	 * Returns a set of all AbstractPersons defined in the input file.
	 * @throws IOException 
	 */
	public Set<AbstractPerson> parse(){
		if(graph == null){
			System.out.println("Null graph in GraphMaker");
			return null;
		} else {
			int id = 0;
			String name = null;
			Set<AbstractPerson> people = new HashSet<AbstractPerson>();

			try {
				reader.beginArray();
				while(reader.hasNext()){
					Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
					int nextId = 0;
					reader.beginObject();
					reader.skipValue(); //skip friends
					reader.beginArray();
					while(reader.hasNext()){
						nextId = reader.nextInt();
						if(graph.containsKey(nextId)){
							friends.add(graph.get(nextId));
						} else {
							AbstractPerson newFriend = new Person(nextId);
							graph.put(nextId, newFriend);
							friends.add(newFriend);
						}
					}
					reader.endArray();
					reader.skipValue();//skip id
					id = reader.nextInt();
					reader.skipValue();//skip name
					name = reader.nextString();
					reader.endObject();
					AbstractPerson newPerson;
					if(graph.containsKey(id)){
						newPerson = graph.get(id);
					} else {
						newPerson = new Person(id);
						graph.put(id, newPerson);
					}
					newPerson.setName(name);
					newPerson.setFriends(friends);
					newPerson.setName(name);
					people.add(newPerson);
				}
				reader.endArray();
				reader.close();
			} catch (IOException e) {
				System.out.println("GraphMaker IOException");
				return null;
			}
			for(AbstractPerson p : people){
				for(AbstractPerson f : p.getFriends()){
					f.addFollower(p);
				}
			}
			return people;
		}
	}
}
