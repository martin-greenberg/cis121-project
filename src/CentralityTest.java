import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;


public class CentralityTest {
	static GraphMaker maker;
	static GraphMaker makerSmall;
	static Set<AbstractPerson> parsed;
	static Set<AbstractPerson> parsedSmall;
	static Map<Integer, Double> btMap;
	static Map<Integer, Double> pgMap0;
	static Map<Integer, Double> pgMap1;
	static Map<Integer, Double> pgMap2;
	static Map<Integer, Double> pgMap3;
	static Map<Integer, Double> pgMap4;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		maker = new GraphMaker("users.json");
		parsed = maker.parse();
		btMap = new HashMap<Integer, Double>();
		pgMap0 = new HashMap<Integer, Double>();
		pgMap1 = new HashMap<Integer, Double>();
		pgMap2 = new HashMap<Integer, Double>();
		pgMap3 = new HashMap<Integer, Double>();
		pgMap4 = new HashMap<Integer, Double>();
		Map<AbstractPerson, Double> outputCentrality = Centrality.betweennessCentrality(parsed);
		makerSmall = new GraphMaker("small.json");
		parsedSmall = makerSmall.parse();
		Map<AbstractPerson, Double> outputPageRank0 = Centrality.pageRank(parsedSmall, 0);
		Map<AbstractPerson, Double> outputPageRank1 = Centrality.pageRank(parsedSmall, 1);
		Map<AbstractPerson, Double> outputPageRank2 = Centrality.pageRank(parsedSmall, 2);
		Map<AbstractPerson, Double> outputPageRank3 = Centrality.pageRank(parsedSmall, 3);
		Map<AbstractPerson, Double> outputPageRank4 = Centrality.pageRank(parsedSmall, 4);
		for(AbstractPerson p : parsed){
			btMap.put(p.getId(), outputCentrality.get(p));
			pgMap0.put(p.getId(), outputPageRank0.get(p));
			pgMap1.put(p.getId(), outputPageRank1.get(p));
			pgMap2.put(p.getId(), outputPageRank2.get(p));
			pgMap3.put(p.getId(), outputPageRank3.get(p));
			pgMap4.put(p.getId(), outputPageRank4.get(p));
		}
	}

	@Test
	public void testBetweennessCentrality() {
		assertEquals(btMap.get(0), .7, .001);
		assertEquals(btMap.get(1), 3.083333333333333, .001);
		assertEquals(btMap.get(2), 0.2, .001);
		assertEquals(btMap.get(3), 0.45, .001);
		assertEquals(btMap.get(4), 0.2, .001);
		assertEquals(btMap.get(5), 3.083333333333333, .001);
		assertEquals(btMap.get(6), 1.2833333333333332, .001);
		assertEquals(btMap.get(7), 0.7499999999999999, .001);
		assertEquals(btMap.get(8), 16.333333333333332, .001);
		assertEquals(btMap.get(9), 0.2, .001);
		assertEquals(btMap.get(10), 2.3666666666666667, .001);
		assertEquals(btMap.get(11), 5.483333333333333, .001);
		assertEquals(btMap.get(12), 8.066666666666666, .001);
		assertEquals(btMap.get(13), 0.6166666666666667, .001);
		assertEquals(btMap.get(14), 0.45, .001);
		assertEquals(btMap.get(15), 0.16666666666666666, .001);
		assertEquals(btMap.get(16), 2.5666666666666664, .001);
		assertEquals(btMap.get(17), 4.733333333333333, .001);
		assertEquals(btMap.get(18), 0.4, .001);
		assertEquals(btMap.get(19), 6.533333333333333, .001);
		assertEquals(btMap.get(20), 0.2, .001);
		assertEquals(btMap.get(21), 0.2, .001);
		assertEquals(btMap.get(22), 0.9, .001);
		assertEquals(btMap.get(23), 0.5, .001);
		assertEquals(btMap.get(24), 1.5333333333333332, .001);
	}

	@Test
	public void testPageRank() {
		assertEquals(pgMap0.get(0), 1.0, 0);
		assertEquals(pgMap0.get(1), 1.0, 0);
		assertEquals(pgMap0.get(2), 1.0, 0);
		assertEquals(pgMap0.get(3), 1.0, 0);
		assertEquals(pgMap0.get(4), 1.0, 0);
		assertEquals(pgMap0.get(5), 1.0, 0);
		assertEquals(pgMap0.get(6), 1.0, 0);
		assertEquals(pgMap0.get(7), 1.0, 0);
		assertEquals(pgMap0.get(8), 1.0, 0);

		assertEquals(pgMap1.get(0), 1.283333333, .001);
		assertEquals(pgMap1.get(1), 1.6375, .001);
		assertEquals(pgMap1.get(2), 0.645833333, .001);
		assertEquals(pgMap1.get(3), 0.3625, .001);
		assertEquals(pgMap1.get(4), 1.070833333, .001);
		assertEquals(pgMap1.get(5), 1, .001);
		assertEquals(pgMap1.get(6), 1, .001);
		assertEquals(pgMap1.get(7), 1, .001);
		assertEquals(pgMap1.get(8), 1, .001);

		assertEquals(pgMap2.get(0), 1.223125, .001);
		assertEquals(pgMap2.get(1), 1.306354167, .001);
		assertEquals(pgMap2.get(2), 0.886666667, .001);
		assertEquals(pgMap2.get(3), 0.422708333, .001);
		assertEquals(pgMap2.get(4), 1.161145833, .001);
		assertEquals(pgMap2.get(5), 1, .001);
		assertEquals(pgMap2.get(6), 1, .001);
		assertEquals(pgMap2.get(7), 1, .001);
		assertEquals(pgMap2.get(8), 1, .001);

		assertEquals(pgMap3.get(0), 1.193271701, .001);
		assertEquals(pgMap3.get(1), 1.459885417, .001);
		assertEquals(pgMap3.get(2), 0.780047743, .001);
		assertEquals(pgMap3.get(3), 0.409914063, .001);
		assertEquals(pgMap3.get(4), 1.156881076, .001);
		assertEquals(pgMap3.get(5), 1, .001);
		assertEquals(pgMap3.get(6), 1, .001);
		assertEquals(pgMap3.get(7), 1, .001);
		assertEquals(pgMap3.get(8), 1, .001);

		assertEquals(pgMap4.get(0), 1.229522135, .001);
		assertEquals(pgMap4.get(1), 1.400978461, .001);
		assertEquals(pgMap4.get(2), 0.817204438, .001);
		assertEquals(pgMap4.get(3), 0.403570237, .001);
		assertEquals(pgMap4.get(4), 1.148724729, .001);
		assertEquals(pgMap4.get(5), 1, .001);
		assertEquals(pgMap4.get(6), 1, .001);
		assertEquals(pgMap4.get(7), 1, .001);
		assertEquals(pgMap4.get(8), 1, .001);
	}

}
