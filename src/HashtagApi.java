import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * A collection of methods that allow for hashtag-based queries over a directed
 * user graph.
 *
 * @author scheiber
 */
public class HashtagApi {
	GraphMaker2 graph;
	/**
	 * Performs the necessary parsing and pre-processing of your graph. This
	 * involves creating a new GraphMaker2 class to handle the more complex
	 * parsing.
	 */
	public HashtagApi(String filename){
		graph = new GraphMaker2(filename);
		graph.parse();
	}

	/**
	 * Returns a set of Integers, each corresponding to the unique Id of a tweet
	 * by the specified person.
	 *
	 * @param person
	 *            if person is null, then throw an IllegalArgumentException
	 */
	public Set<Integer> getTweetsByPerson(AbstractPerson person) {
		if(person == null){
			throw new IllegalArgumentException();
		}
		if(graph == null){
			return new HashSet<Integer>();
		}
		Set<Integer> output = graph.getTweetsByPersonMap().get(person.getId());
		if(output == null){
			return new HashSet<Integer>(); //return empty set instead of null output if person isn't in graph
		}
		return output; //otherwise return the retrieved set
	}

	/**
	 * Returns a set of Integers, each corresponding to the unique Id of a tweet
	 * containing the specified hashtag.
	 *
	 * @param hashtag
	 *            is non-null and begins with a pound sign
	 */
	public Set<Integer> getTweetsWithHashtag(String hashtag) {
		if((hashtag == null) || (hashtag == "") || (!(hashtag.toCharArray()[0] == 35)) || (graph == null)){
			return new HashSet<Integer>();
		}
		Set<Integer> hashtags = graph.getTweetsByHashtagMap().get(hashtag.substring(1));
		if(hashtags == null){
			return new HashSet<Integer>();
		} else {
			return hashtags;
		}
	}
	
	GraphMaker2 getGraphObject(){
		return this.graph;
	}
}
