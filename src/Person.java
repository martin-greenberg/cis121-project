import java.util.HashSet;
import java.util.Set;


public class Person extends AbstractPerson implements Comparable<Person> {
	private Set<AbstractPerson> friends;
	private Set<AbstractPerson> followers;
	private String name;
	public Person(int id){
		super(id);
		this.friends = new HashSet<AbstractPerson>();
		this.followers = new HashSet<AbstractPerson>();
		this.name = "";
	}
	@Override
	public Set<AbstractPerson> getFriends() {
		return this.friends;
	}

	@Override
	public void setFriends(Set<AbstractPerson> friends){
		if(friends == null){
			throw new IllegalArgumentException();
		}
		this.friends = friends;
	}

	@Override
	public void addFriend(AbstractPerson friend) {
		if((friend == null) || (friend == this)){
			throw new IllegalArgumentException();
		}
		this.friends.add(friend);
	}

	@Override
	public void removeFriend(AbstractPerson friend) {
		if((friend == null) || (!this.friends.contains(friend))){
			throw new IllegalArgumentException();
		}
		this.friends.remove(friend);
	}

	@Override
	public Set<AbstractPerson> getFollowers() {
		return this.followers;
	}

	@Override
	public void setFollowers(Set<AbstractPerson> followers) {
		if(followers == null){
			throw new IllegalArgumentException();
		}
		this.followers = followers;
	}

	@Override
	public void addFollower(AbstractPerson follower) {
		if((follower == null) || (follower == this)){
			throw new IllegalArgumentException();
		}
		this.followers.add(follower);
	}

	@Override
	public void removeFollower(AbstractPerson follower) {
		if((follower == null) || (!this.followers.contains(follower))){
			throw new IllegalArgumentException();
		}
		this.followers.remove(follower);
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		if(name == null){
			throw new IllegalArgumentException();
		}
		this.name = name;
	}
	@Override
	public int compareTo(Person o) {
		return(this.getId()-o.getId());
	}

}
