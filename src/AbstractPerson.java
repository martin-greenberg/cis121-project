import java.util.Set;

/**
 * AbstractPerson is an abstract class representing a node in a graph of a
 * social network. You will extend this class to create your own Person
 * implementation.
 * 
 * DO NOT MODIFY THIS CLASS
 * 
 * @author saajid, nagornyi
 */
public abstract class AbstractPerson {

	private final int id;

	public AbstractPerson(int id) {
		this.id = id;
	}

	/**
	 * Returns a set of AbstractPersons to which this AbstractPerson is
	 * connected.
	 */
	public abstract Set<AbstractPerson> getFriends();

	/**
	 * @param friends
	 *            friends is the set of AbstractPersons given as friends to this
	 *            AbstractPerson. Previously held friends should no longer
	 *            belong to this AbstractPerson. DO NOT iterate over the friends
	 *            and add this AbstractPerson as a follower to their followers
	 *            set. If friends is null, then throw IllegalArgumentException.
	 */
	public abstract void setFriends(Set<AbstractPerson> friends);

	/**
	 * 
	 * @param friend
	 *            DO NOT add this AbstractPerson as a follower to the specified
	 *            friend's followers set. If friend is null, then throw an
	 *            IllegalArgumentException.
	 */
	public abstract void addFriend(AbstractPerson friend);

	/**
	 * 
	 * Removes a AbstractPerson from this AbstractPerson's friends
	 * 
	 * @param friend
	 *            DO NOT remove this AbstractPerson as a follower from the
	 *            specified friend's followers set. If friend is null or is not
	 *            a friend of this AbstractPerson, then throw an
	 *            IllegalArgumentException.
	 */
	public abstract void removeFriend(AbstractPerson friend);

	/**
	 * Returns a set of AbstractPersons that follow this AbstractPerson.
	 */
	public abstract Set<AbstractPerson> getFollowers();

	/**
	 * @param followers
	 *            the set of AbstractPersons given as inbound connections to
	 *            this AbstractPerson. Previously held followers should no
	 *            longer belong to this AbstractPerson. DO NOT iterate over the
	 *            followers set and add this AbstractPerson as a friend to their
	 *            friends set. If followers is null, then throw an
	 *            IllegalArgumentException.
	 */
	public abstract void setFollowers(Set<AbstractPerson> followers);

	/**
	 * @param follower
	 *            DO NOT add this AbstractPerson as a friend to the specified
	 *            friend's followers set. If follower is null, then throw an
	 *            IllegalArgumentException.
	 */
	public abstract void addFollower(AbstractPerson follower);

	/**
	 * Removes a AbstractPerson from this AbstractPerson's friends
	 * 
	 * @param friend
	 *            DO NOT remove this AbstractPerson from the specified friend's
	 *            followers set. If friend is null or does not follow this
	 *            AbstractPerson, then throw an IllegalArgumentException.
	 */
	public abstract void removeFollower(AbstractPerson follower);

	/**
	 * Returns the name of this AbstractPerson.
	 */
	public abstract String getName();

	/**
	 * @param name
	 *            the name that will be given to this AbstractPerson
	 */
	public abstract void setName(String name);

	/**
	 * Returns the Id of this AbstractPerson. Each AbstractPerson must have a
	 * unique Id.
	 */
	public int getId() {
		return this.id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AbstractPerson)) {
			return false;
		}
		AbstractPerson other = (AbstractPerson) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

}
