import java.util.HashSet;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.Stack;

/**
 * ConnectedComponents is a class that processes a graph.
 * 
 * Do not change method headers that are given to you. Be careful about
 * modifying the graphs that are passed into your methods. Make sure you
 * understand what you are referencing in your algorithms and tests.
 * 
 * @author saajid
 */
public class ConnectedComponents {

	/**
	 * Returns an iterator (defined as an inner class) of the reachable nodes in
	 * the graph from the source node. Nodes are returned in depth-first order
	 * where edges are traversed in increasing order by Ids of the target
	 * AbstractPersons. Your iterator should be lazy; it should not prepare all
	 * the elements to be returned by next() until they are needed (hasNext()
	 * should have similar behavior). This method should return an instance of
     * an inner class. Make sure not to pop AbstractPersons from your stack
     * until they are required by a call to next() or hasNext().
	 * 
	 * Your iterator should return an UnsupportedOperationException if the
	 * remove() method is called.
	 * 
	 * @param root
	 *            - if root is null, then throw IllegalArgumentException
	 */
	public static Iterator<AbstractPerson> dfsIterator(AbstractPerson root) {
		if(root == null){
			throw new IllegalArgumentException();
		}
		class dfsIterInner implements Iterator<AbstractPerson>{
			private Set<AbstractPerson> marked;
			private Set<AbstractPerson> returned;
			private Stack<AbstractPerson> personStack;
			private PriorityQueue<AbstractPerson> edges;
			
			dfsIterInner(AbstractPerson rootInner){
				marked = new HashSet<AbstractPerson>();
				personStack = new Stack<AbstractPerson>();
				personStack.add(rootInner);
				returned = new HashSet<AbstractPerson>();
				
			}
			@Override
			public boolean hasNext() {
				while(!personStack.isEmpty()){ //pop through finished vertices on the stack until we find an unfinished one
					edges = new PriorityQueue<AbstractPerson>();
					AbstractPerson testVertex = personStack.peek();
					for(AbstractPerson p: testVertex.getFriends()){
						if((!marked.contains(p)) && (!personStack.contains(p))){
							edges.add(p); //priority queue minheap of unmarked adjacent vertices
						}
					}
					if(edges.isEmpty() && !returned.isEmpty()){ //vertex on the top of the stack is done and not the root, so mark and pop it and keep looking
						marked.add(personStack.pop());
					} else {
						return true; //if testVertex has unmarked adjacent vertices then return the least of them
					}
				}
				return false;
			}

			@Override
			public AbstractPerson next(){
				if(!this.hasNext()){
					return null;
				} else { //otherwise an unfinished vertex is now on the top of the stack with at least one white vertex in the edges pq
					if(returned.size() == 0){ //return the root first
						returned.add(personStack.peek());
						return personStack.peek();
					}
					AbstractPerson nextVertex = edges.poll();
					personStack.add(nextVertex); //push unmarked adjacent vertex with lowest ID onto stack
					return nextVertex;
					}
				}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		}
		return new dfsIterInner(root);
	}

	/**
	 * Returns the maximal weakly connected component of the graph.
	 * 
	 * @param graph
	 *            - If graph is null, then throw an IllegalArgumentException
	 */
	public static Set<AbstractPerson> weaklyConnectedComponent(
			Set<AbstractPerson> graph) {
		if(graph == null){
			throw new IllegalArgumentException();
		}
		Set<AbstractPerson> wcc = new HashSet<AbstractPerson>();
		for(AbstractPerson p: graph){
			Set<AbstractPerson> candidateMaxWcc = new HashSet<AbstractPerson>();
			Iterator<AbstractPerson> dfs = dfsIterator(p);
			while(dfs.hasNext()){
				candidateMaxWcc.add(dfs.next());
			}
			if(candidateMaxWcc.size() > wcc.size()){
				wcc = candidateMaxWcc;
			}			
		}
		return wcc;
	}
	
	/**
	 * Returns the maximal strongly connected component of the graph.
	 * 
	 * @param graph
	 *            - If graph is null, then throw an IllegalArgumentException.
	 */
	public static Set<AbstractPerson> kosaraju(Set<AbstractPerson> graph) {
		if(graph == null){
			throw new IllegalArgumentException();
		}
		Set<AbstractPerson> graphReverse = new HashSet<AbstractPerson>();
		Stack<AbstractPerson> reversePost = new Stack<AbstractPerson>();
		for(AbstractPerson p : graph){
			AbstractPerson reverse = new Person(p.getId());
			Set<AbstractPerson> temp = p.getFollowers();
			reverse.setFollowers(p.getFriends());
			reverse.setFriends(temp);
			graphReverse.add(reverse);
		}
		
		
		while(reversePost.size() < graph.size()){ 
			for(AbstractPerson p : graph){
				if(!reversePost.contains(p)){ //start DFS at p -- ensures that the reverse postorder contains all vertices even if graph is not connected
					HashSet<AbstractPerson> marked = new HashSet<AbstractPerson>();
					Stack<AbstractPerson> personStack = new Stack<AbstractPerson>();
					personStack.add(p); //put root on the stack
					while(!personStack.empty()){ //pop through finished vertices on the stack until it finds an unfinished one
						AbstractPerson testVertex = personStack.peek();
						for(AbstractPerson f: testVertex.getFriends()){
							if((!marked.contains(f)) && (!personStack.contains(f))){
								personStack.add(f);
								break;
							}
						}
						//if got to here then vertex on the top of the stack is done, mark and pop it to the reverse postorder stack
						AbstractPerson finishedVertex = personStack.pop();
						marked.add(finishedVertex);
						reversePost.add(finishedVertex);
					}
				}
			}
		}
		Set<AbstractPerson> maxSCC = new HashSet<AbstractPerson>();
		Set<AbstractPerson> candidateMaxSCC;
		while(!reversePost.isEmpty()){
			candidateMaxSCC = new HashSet<AbstractPerson>();
			Iterator<AbstractPerson> sccIter = dfsIterator(reversePost.pop());
			while(sccIter.hasNext()){
				candidateMaxSCC.add(sccIter.next());
			}
			if(candidateMaxSCC.size() > maxSCC.size()){
				maxSCC = candidateMaxSCC;
			}
		}
		return maxSCC;
		}
	}
