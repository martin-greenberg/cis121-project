import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class ConnectedComponentsTest {
	private static Set<AbstractPerson> graph1Node;
	private static Set<AbstractPerson> graphEmpty;
	private static Set<AbstractPerson> graphSimple;
	private static Set<AbstractPerson> graphSimpleTwoComponents; //component with most nodes is largest SCC
	private static Set<AbstractPerson> graphSimpleTwoComponentsB; //different largest SCC
	private static Set<AbstractPerson> graphNull;
	static AbstractPerson oneNode;
	static AbstractPerson a;
	static AbstractPerson b;
	static AbstractPerson c;
	static AbstractPerson d;
	static AbstractPerson e;
	static AbstractPerson f;
	static AbstractPerson g;
	static AbstractPerson h;
	static AbstractPerson i;
	static AbstractPerson j;
	
	@BeforeClass
	public static void setUpClass() throws Exception {
		a = new Person(0);
		b = new Person(1);
		c = new Person(2);
		d = new Person(3);
		e = new Person(4);
		f = new Person(5);
		g = new Person(6);
		h = new Person(7);
		i = new Person(8);
		j = new Person(9);
		oneNode = new Person(10);
		
		a.addFollower(b);
		b.addFriend(a);
		a.addFollower(d);
		d.addFriend(a);
		a.addFriend(c);
		c.addFollower(a);
		c.addFriend(b);
		b.addFollower(c);
		c.addFriend(d);
		d.addFollower(c);
		
		e.addFollower(f);
		f.addFriend(e);
		e.addFriend(f);
		f.addFollower(e);
		
		graph1Node = new HashSet<AbstractPerson>();
		graph1Node.add(oneNode);
		
		graphEmpty = new HashSet<AbstractPerson>();
		
		graphSimple = new HashSet<AbstractPerson>();
		graphSimple.add(a);
		graphSimple.add(b);
		graphSimple.add(c);
		graphSimple.add(d);
		
		graphSimpleTwoComponents = new HashSet<AbstractPerson>();
		graphSimpleTwoComponents.addAll(graphSimple);
		graphSimpleTwoComponents.add(e);
		graphSimpleTwoComponents.add(f);
		

		graphSimpleTwoComponentsB = new HashSet<AbstractPerson>();
		graphSimpleTwoComponentsB.add(e);
		graphSimpleTwoComponentsB.add(f);
		
		g.addFriend(h);
		i.addFriend(h);
		h.addFollower(g);
		h.addFollower(i);
		j.addFollower(e);
		j.addFriend(f);
		graphSimpleTwoComponentsB.add(g);
		graphSimpleTwoComponentsB.add(h);
		graphSimpleTwoComponentsB.add(i);
		graphSimpleTwoComponentsB.add(j);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void dfsIteratorTestNull() {
		Iterator<AbstractPerson> iter = ConnectedComponents.dfsIterator(null);
	}
	
	@Test
	public void dfsIteratorTest1() {
		Iterator<AbstractPerson> iter = ConnectedComponents.dfsIterator(a);
		assertEquals(iter.next(), a);
		assertEquals(iter.next(), c);
		assertEquals(iter.next(), b);
		assertEquals(iter.next(), d);
		assertFalse(iter.hasNext());
		//Set<AbstractPerson> aDFSOut = new HashSet<AbstractPerson>();
		//while(iter.hasNext()){
		//	aDFSOut.add(iter.next());
		//}
	}
	
	@Test
	public void dfsIteratorTestOneNode() {
		Iterator<AbstractPerson> iter = ConnectedComponents.dfsIterator(oneNode);
		assertEquals(iter.next(), oneNode);
		assertFalse(iter.hasNext());
	}
	
	@Test
	public void wccTest1Node() {
		Iterator<AbstractPerson> iter = ConnectedComponents.weaklyConnectedComponent(graph1Node).iterator();
		assertEquals(iter.next(),oneNode);
		assertFalse(iter.hasNext());
	}
	
	@Test
	public void wccTestEmpty() {
		Iterator<AbstractPerson> iter = ConnectedComponents.weaklyConnectedComponent(graphEmpty).iterator();
		assertFalse(iter.hasNext());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void wccTestNullGraph() {
		ConnectedComponents.weaklyConnectedComponent(graphNull);
	} 
	
	@Test 
	public void wccTestSimple() {
		Set<AbstractPerson> output = ConnectedComponents.weaklyConnectedComponent(graphSimple);
		Set<AbstractPerson> expected = new HashSet<AbstractPerson>();
		expected.add(a);
		expected.add(b);
		expected.add(c);
		expected.add(d);
		assertEquals(output, expected);
	} 
	
	@Test 
	public void wccTestTwoComponents() {
		Set<AbstractPerson> output = ConnectedComponents.weaklyConnectedComponent(graphSimpleTwoComponents);
		Set<AbstractPerson> expected = new HashSet<AbstractPerson>();
		expected.add(a);
		expected.add(b);
		expected.add(c);
		expected.add(d);
		assertEquals(output, expected);
	} 
	
	@Test
	public void sccTest1Node() {
		Iterator<AbstractPerson> iter = ConnectedComponents.kosaraju(graph1Node).iterator();
		assertEquals(iter.next(),oneNode);
		assertFalse(iter.hasNext());
	}
	
	@Test
	public void sccTestEmpty() {
		Iterator<AbstractPerson> iter = ConnectedComponents.kosaraju(graphEmpty).iterator();
		assertFalse(iter.hasNext());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void sccTestNullGraph() {
		ConnectedComponents.kosaraju(graphNull);
	} 
	
	@Test 
	public void sccTestSimple() {
		Set<AbstractPerson> output = ConnectedComponents.kosaraju(graphSimple);
		Set<AbstractPerson> expected = new HashSet<AbstractPerson>();
		expected.add(a);
		expected.add(b);
		expected.add(c);
		expected.add(d);
		assertEquals(output, expected);
	} 
	
	@Test 
	public void sccTestTwoComponents() {
		Set<AbstractPerson> output = ConnectedComponents.kosaraju(graphSimpleTwoComponentsB);
		Set<AbstractPerson> expected = new HashSet<AbstractPerson>();
		expected.add(e);
		expected.add(f);
		expected.add(j);
		assertEquals(output, expected);
	}
}
