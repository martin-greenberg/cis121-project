import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.gson.stream.JsonReader;

/**
 * A class that parses a data file in JSON form into a graph representation.
 * 
 * @author wangrace
 * 
 */
public class GraphMaker2 {
	private Map<Integer,AbstractPerson> graph;
	private Map<Integer, String> tweets;
	private Map<Integer, Set<Integer>> tweetsByPerson;
	private Map<String, Set<Integer>> tweetsByHashtag;
	JsonReader reader;
	
	public GraphMaker2(String filename){
		if(filename == null){
			System.out.println("GraphMaker2: null filename string");
		} else {
			graph = new HashMap<Integer,AbstractPerson>();
			try {
				reader = new JsonReader(new FileReader(filename));
			} catch (FileNotFoundException e) {
				System.out.println("GraphMaker2: file not found");
			}
			tweets = new HashMap<Integer, String>();
			tweetsByPerson = new HashMap<Integer, Set<Integer>>();
			tweetsByHashtag = new HashMap<String, Set<Integer>>();
		}
	}

	/**
	 * Returns a set of all AbstractPersons defined in the input file.
	 * 
	 */
	public Set<AbstractPerson> parse(){
		if(reader == null){
			System.out.println("GraphMaker2: null reader");
			return null;
		}
		int id = 0;
		String name = null;
		Set<AbstractPerson> people = new HashSet<AbstractPerson>();
		try {
			reader.beginArray();
			while(reader.hasNext()){
				Set<AbstractPerson> friends = new HashSet<AbstractPerson>();
				Set<Integer> tweetSet = new HashSet<Integer>(); //person's tweets
				int nextId = 0;
				reader.beginObject();
				reader.skipValue(); //skip tweets
				reader.beginArray(); //begin tweets
				while(reader.hasNext()){
					reader.beginObject(); //begin tweet object
					reader.skipValue();// skip text;
					String text = reader.nextString();
					reader.skipValue(); //skip id
					Integer tweetId = reader.nextInt();
					tweets.put(tweetId, text);
					for(String s : this.extractHashtags(text)){
						if(tweetsByHashtag.containsKey(s)){
							tweetsByHashtag.get(s).add(tweetId);
						} else {
							Set<Integer> tweetsForHashtag = new HashSet<Integer>();
							tweetsForHashtag.add(tweetId);
							tweetsByHashtag.put(s, tweetsForHashtag);
						}
					}
					tweetSet.add(tweetId);
					reader.endObject(); //end tweet object
				}
				reader.endArray(); //end tweet array
				reader.skipValue(); //skip friends
				reader.beginArray();
				while(reader.hasNext()){
					nextId = reader.nextInt();
					if(graph.containsKey(nextId)){
						friends.add(graph.get(nextId));
					} else {
						AbstractPerson newFriend = new Person(nextId);
						graph.put(nextId, newFriend);
						friends.add(newFriend);
					}
				}
				reader.endArray();
				reader.skipValue();//skip id
				id = reader.nextInt();
				reader.skipValue();//skip name
				name = reader.nextString();
				reader.endObject();
				AbstractPerson newPerson;
				if(graph.containsKey(id)){
					newPerson = graph.get(id);
				} else {
					newPerson = new Person(id);
					graph.put(id, newPerson);
				}
				newPerson.setName(name);
				newPerson.setFriends(friends);
				newPerson.setName(name);
				people.add(newPerson);
				tweetsByPerson.put(id, tweetSet);
			}
			reader.endArray();
			reader.close();
		} catch (IOException e) {
			System.out.println("GraphMaker2: IOException");
			return null;
		}
		for(AbstractPerson p : people){
			for(AbstractPerson f : p.getFriends()){
				f.addFollower(p);
			}
		}
		return people;
	}
	
	static Set<String> extractHashtags(String s){
		String tweet = s;
		Set<String> tags = new HashSet<String>();
		if(s == null){
			return tags;
		}
		while(tweet.length() != 0){
			int hashtagIndex = tweet.indexOf(35);
			if(hashtagIndex == -1){ //no more hashtags
				return tags;
			}
			tweet = tweet.substring(hashtagIndex+1);
			int currIndex = 0;
			while(Character.isLetterOrDigit(tweet.toCharArray()[currIndex])){
				currIndex++;
				if(currIndex == tweet.length()) break;
			}
			tags.add(tweet.substring(0, currIndex));
			tweet = tweet.substring(currIndex);
		}
		return tags;
	}
	
	public  Map<Integer,AbstractPerson> getGraphMap(){
		return this.graph;
	}
	public Map<Integer, String> getTweetsMap(){
		return this.tweets;
	}
	public Map<Integer, Set<Integer>> getTweetsByPersonMap(){
		return this.tweetsByPerson;
	}
	public Map<String, Set<Integer>> getTweetsByHashtagMap(){
		return this.tweetsByHashtag;
	}
}