import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;


public class PersonTest {
	AbstractPerson alice;
	AbstractPerson bob;
	AbstractPerson carl;
	
	@Before
	public void setUp() throws Exception {
		alice = new Person(1);
		bob = new Person(2);
		carl = new Person(3);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSetFriendsNullInput() {
		HashSet<AbstractPerson> friendSet = null;
		alice.setFriends(friendSet);
	}
	
	@Test
	public void testSetFriendsValidInput() {
		HashSet<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(bob);
		alice.setFriends(friends);
		assertEquals(alice.getFriends().size(), 1);
		assertTrue(alice.getFriends().contains(bob));
	}
	
	@Test
	public void testGetFriendsWithNone() {
		assertArrayEquals(alice.getFriends().toArray(), (new HashSet<AbstractPerson>()).toArray());
	}
	
	@Test
	public void testGetFriendsWithTwo() {
		HashSet<AbstractPerson> friends = new HashSet<AbstractPerson>();
		friends.add(bob);
		friends.add(carl);
		alice.setFriends(friends);
		assertEquals(alice.getFriends().size(), 2);
		assertTrue(alice.getFriends().contains(bob));
		assertTrue(alice.getFriends().contains(carl));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddFriendNoSelfLoops() {
		alice.addFriend(alice);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddFriendNull() {
		alice.addFriend(null);
	}
	
	@Test
	public void testAddFriendValid() {
		alice.addFriend(bob);
		assertEquals(alice.getFriends().size(), 1);
		assertTrue(alice.getFriends().contains(bob));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testRemoveFriendNull() {
		alice.removeFriend(null);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testRemoveFriendNotAFriend() {
		alice.removeFriend(bob);
	}
	
	@Test
	public void testRemoveFriendValid() {
		alice.addFriend(bob);
		assertTrue(alice.getFriends().contains(bob));
		alice.removeFriend(bob);
		assertFalse(alice.getFriends().contains(bob));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSetFollowersNull() {
		alice.setFollowers(null);
	}
	
	@Test
	public void testSetFollowersValid() {
		HashSet<AbstractPerson> followers = new HashSet<AbstractPerson>();
		followers.add(bob);
		followers.add(carl);
		alice.setFollowers(followers);
		assertEquals(alice.getFollowers().size(), 2);
		assertTrue(alice.getFollowers().contains(bob));
		assertTrue(alice.getFollowers().contains(carl));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddFollowerNoSelfLoops() {
		alice.addFollower(alice);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddFollowerNull() {
		alice.addFollower(null);
	}
	
	@Test
	public void testAddFollowerValid() {
		alice.addFollower(bob);
		assertEquals(alice.getFollowers().size(),1);
		assertTrue(alice.getFollowers().contains(bob));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testRemoveFollowerNull() {
		alice.removeFollower(null);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testRemoveFollowerNotAFollower() {
		alice.removeFollower(bob);
	}
	
	@Test
	public void testRemoveFollowerValid() {
		alice.addFollower(bob);
		assertTrue(alice.getFollowers().contains(bob));
		alice.removeFollower(bob);
		assertFalse(alice.getFollowers().contains(bob));
	}
	
	@Test
	public void testGetNameNone(){
		assertEquals(alice.getName(), "");
	}
	
	@Test
	public void testSetGetName() {
		alice.setName("Alice");
		assertEquals(alice.getName(), "Alice");
	}
	
	@Test
	public void testGetID() {
		assertEquals(alice.getId(), 1);
	}
}
